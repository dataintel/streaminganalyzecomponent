
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-streaminganalyze',
  template: `<style> /* For the "inset" look only */



html {
    overflow: auto;

}

body {
    position: absolute;
    top: 20px;
    left: 20px;
    bottom: 20px;
    right: 20px;
    padding: 30px; 
    overflow-y: scroll;
    overflow-x: hidden;
    
}

/* Let's get this party started */
::-webkit-scrollbar {
    width: 12px;
}
 
/* Track */
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); 
    -webkit-border-radius: 10px;
    border-radius: 10px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
    -webkit-border-radius: 10px;
    border-radius: 10px;
    background: slategrey; 
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 
}
::-webkit-scrollbar-thumb:window-inactive {
	background: gray; 
}

md-list-item{
    margin: 2px 2px 2px 2px;
    margin-left: -13px;
    margin-right: -13px;
}</style>

<div id="utama" class="col-md-6" style="max-height:300px;overflow-y:scroll;">
  <md-list>
    <div *ngFor ="let datacarrier of dataLoader">
      <md-list-item >
      

        <p md-line>
          <span>{{datacarrier.title}}</span>
        </p>
        <h3 md-line><small style="font-size:12px;">{{datacarrier.media_displayName}} . {{datacarrier.timestamp}} </small></h3>
      </md-list-item>
       <md-divider></md-divider>
    </div>
   
  </md-list>
</div>`
})
export class BaseWidgetComponetStreamingAnalyze implements OnInit {
  public dataLoader: Array<any> = [
	 {
      "title" : "British Museum show charts America pop art",
      "timestamp" : "17 days ago",
      "media_displayName":"The Malay Mail Online"
    },
    {
      "title" : "Exhibition charting American pop art opens at London's British Museum",
      "timestamp" : "17 days ago",
      "media_displayName":"The Malaysian Times"
  },
    {
      "title" : "Exhibition charting American pop art opens at London's British Museum",
      "timestamp" : "17 days ago",
      "media_displayName":"Channel News Asia"
     },
    {
       "title" : "Exhibition charting American pop art opens at London's British Museum",
      "timestamp" : "17 days ago",
      "media_displayName":"Channel News Asia"
    },
    {
     "title" : "Exhibition charting American pop art opens at London's British Museum",
      "timestamp" : "17 days ago",
      "media_displayName":"Channel News Asia"
     }];

    

  constructor() {console.log(this.dataLoader);}
  
  ngOnInit() { 
  }

}