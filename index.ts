import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetComponetStreamingAnalyze } from './src/base-widget.component';
//import { MaterialModule } from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '@angular/material';

export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule,
	MaterialModule,
    BrowserAnimationsModule
  ],
  declarations: [
	BaseWidgetComponetStreamingAnalyze 
  ],
  exports: [
	BaseWidgetComponetStreamingAnalyze 
  ]
})
export class StreamingAnalyzeModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: StreamingAnalyzeModule,
      providers: []
    };
  }
}
