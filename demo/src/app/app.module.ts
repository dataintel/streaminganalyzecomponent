import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule } from '@angular/material';
// import { BaseWidgetComponent } from './base-widget/base-widget.component';
import 'hammerjs';
import {StreamingAnalyzeModule} from '../../../index';


@NgModule({
  declarations: [
    AppComponent,
    // BaseWidgetComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MaterialModule,
    StreamingAnalyzeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
